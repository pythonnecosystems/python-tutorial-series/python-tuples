# Python 튜플: 생성, 액세스 및 메소드 <sup>[1](#footnote_1)</sup>

> <font size="3">불변의 컬렉션인 튜플을 만들고 액세스하는 방법에 대해 알아본다.</font>

## 목차

1. [소개](./tuples.md#intro)
1. [튜플이란? 왜 필요한가?](./tuples.md#sec_02)
1. [Python에서 튜플 생성](./tuples.md#sec_03)
1. [튜플 요소 액세스 방법](./tuples.md#sec_04)
1. [튜플에서 반복 방법](./tuples.md#sec_05)
1. [튜플 메소드와 연산](./tuples.md#sec_06)
1. [맺는말](./tuples.md#conclusion)

<a name="footnote_1">1</a>: [Python Tutorial 10 — Python Tuples: Creation, Access, and Methods](https://levelup.gitconnected.com/python-tutorial-10-python-tuples-creation-access-and-methods-b956437d554f?sk=cbd8190f13757783eede04e0a03bd35c)를 편역한 것이다.