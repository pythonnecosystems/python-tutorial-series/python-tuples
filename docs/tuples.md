# Python 튜플: 생성, 액세스 및 메소드

## <a name="intro"></a> 소개
이 포스팅에서는 Python에서 튜플을 만들고 액세스하는 방법에 대해 설명할 것이다. 튜플은 Python에서 리스트, 사전, 집합과 함께 기본적인 데이터 구조 중 하나이다. 좌표, 날짜, 이름 등과 같은 여러 값을 하나의 변수에 저장하는 데 사용된다.

그러나 튜플은 리스트와 달리 불변이기 때문에 일단 생성되면 변경할 수 없다. 따라서 튜플은 상수, 구성 설정 또는 레코드와 같이 수정해서는 안 되는 데이터를 저장하는 데 유용하다.

이 포스팅의 학습이 끝나면 다음 작업을 수행할 수 있다.

- 다른 메서드를 사용하여 튜플 만들기
- 인덱싱과 슬라이싱을 사용하여 튜플 요소 액세스
- 루프와 컴프리헨션으로 튜플 반복하기
- 튜플 메소드와 연산으로 튜플을 조작하고 비교하기

시작할 준비 됐나요? 어서 시작하자!

## <a name="sec_02"></a> 튜플이란? 왜 필요한가?
튜플(tuple)은 순서와 불변의 값들의 컬렉션이다. 즉, 튜플은 생성된 후에는 변경할 수 없는 항목의 연속이다. 튜플은 숫자, 문자열, 부울, 목록, 사전 등과 같은 모든 타입의 데이터를 포함할 수 있다. 튜플은 또한 다른 튜플을 포함할 수 있는데, 이를 중첩 튜플이라고 한다.

그런데 왜 리스트 대신 튜플을 사용할가? Python 프로그래밍에서 튜플이 유용한 이유는 다음과 같다.

- 투플은 메모리 사용량이 적어 더 빠르게 접근할 수 있기 때문에 목록보다 빠르고 효율적이다.
- 튜플은 변경해서는 안 되는 데이터를 실수로 수정하는 것을 방지하기 때문에 리스트보다 더 안전하고 신뢰할 수 있다.
- 튜플은 해시 가능하고 고유하기 때문에 사전에서 키로 사용될 수 있다. 반면에 리스트는 해시 불가능하고 변경할 수 있기 때문에 키로 사용될 수 없다.
- 튜플은 값을 쉽게 포장하고 풀 수 있기 때문에 함수에서 여러 값을 반환하는 데 사용될 수 있다.

보다시피 튜플은 특정 상황에서 리스트보다 장점을 가지고 있다. 그러나 튜플은 다음과 같은 제한도 가지고 있다.

- 튜플은 일단 생성되면 수정, 추가, 제거할 수 없다. 즉, 튜플의 크기나 내용을 변경할 수 없다.
- 튜플은 불변이기 때문에 리스트보다 방법과 연산이 적다. 이는 리스트와 같은 방식으로 튜플을 정렬하거나 되돌리거나 조작할 수 없다는 것을 의미한다.

따라서 고정적이고 일정한 데이터를 저장할 때 튜플을 사용하고 동적이고 가변적인 데이터를 저장할 때 목록을 사용해야 한다.

이제 튜플이 무엇이고 왜 튜플을 사용하는지 알았으니 Python에서 튜플을 만드는 방법을 알아보자.

## <a name="sec_03"></a> Python에서 튜플 생성
Python에서 튜플을 만드는 여러 가지 방법이 있다. 가장 일반적인 방법은 괄호(`()`)를 사용하고 쉼표로 값을 구분하는 것이다. 예를 들어, 다음과 같이 수들의 튜플을 만들 수 있다.

```python
# Create a tuple of numbers
numbers = (1, 2, 3, 4, 5)
print(numbers)
# Output: (1, 2, 3, 4, 5)
```

문자열, 부울, 또는 다른 타입의 데이터로 구성된 튜플을 만들 수도 있다. 예를 들어, 다음과 같은 이름들의 튜플을 만들 수 있다.

```python
# Create a tuple of names
names = ("Alice", "Bob", "Charlie")
print(names)
# Output: ("Alice", "Bob", "Charlie")
```

숫자나 문자열과 같은 혼합 타입의 튜플을 만들 수도 있다. 예를 들어, 다음과 같은 좌표 튜플을 만들 수 있다.

```python
# Create a tuple of coordinates
coordinates = (10, 20, "North")
print(coordinates)
# Output: (10, 20, "North")
```

또한 튜플의 튜플을 만들 수 있는데, 이를 중첩 튜플이라고 한다. 예를 들어, 다음과 같은 날짜의 튜플을 만들 수 있다.

```python
# Create a tuple of dates
dates = ((2023, 1, 1), (2023, 2, 14), (2023, 3, 17))
print(dates)
# Output: ((2023, 1, 1), (2023, 2, 14), (2023, 3, 17))
```

Python에서 튜플을 만드는 또 다른 방법은 내장 함수 `tuple()`을 사용하는 것인데, 이는 반복 가능한 객체를 인수로 받아 튜플을 반환한다. 반복 가능한 객체는 리스트, 문자열, range 등과 같이 반복할 수 있는 모든 객체이다. 예를 들어, 다음과 같이 리스트에서 튜플을 만들 수 있다.

```python
# Create a tuple from a list
list = [6, 7, 8, 9, 10]
tuple = tuple(list)
print(tuple)
# Output: (6, 7, 8, 9, 10)
```

다음과 같이 문자열에서 튜플을 만들 수도 있다.

```python
# Create a tuple from a string
string = "Hello"
tuple = tuple(string)
print(tuple)
# Output: ("H", "e", "l", "l", "o")
```

다음과 같이 range에서 튜플을 만들 수도 있다.

```python
# Create a tuple from a range
range = range(1, 11)
tuple = tuple(range)
print(tuple)
# Output: (1, 2, 3, 4, 5, 6, 7, 8, 9, 10)
```

한 가지 주의할 점은 요소 하나로 튜플을 만들려면 요소 뒤에 쉼표를 추가해야 하며, 그렇지 않으면 튜플로 인식되지 않는다는 것이다. 예를 들어, 다음과 같이 요소 하나로 튜플을 만들 수 있다.

```python
# Create a tuple with one element
single = (42,)
print(single)
# Output: (42,)
```

그러나 쉼표를 생략하면 정규 값으로 처리된다. 예를 들어, 다음 코드는 튜플을 만들지 않는다.

```python
# This is not a tuple
not_a_tuple = (42)
print(not_a_tuple)
# Output: 42
```

따라서 한 요소로 튜플을 만들고 싶다면 쉼표를 추가하는 것을 잊지 마십시오.

이제 Python에서 다양한 방법을 사용하여 튜플을 만드는 방법을 알게 되었다. 다음은 튜플 요소에 접근하는 방법을 알아보자.

## <a name="sec_04"></a> 튜플 요소 액세스 방법
튜플 요소를 액세스하기 위해서는 인덱싱과 슬라이싱을 사용할 수 있다. 인덱싱은 튜플의 위치를 이용하여 단일 요소에 접근하는 과정이다. 슬라이싱은 튜플의 위치를 이용하여 다양한 요소에 접근하는 과정이다.

인덱싱과 슬라이싱은 투플의 경우 리스트와 동일하게 작동한다. 대괄호 `[]`와 숫자를 사용하여 인덱스로 요소를 액세스할 수 있다. 인덱스는 첫 번째 요소의 경우 0부터 시작하여 마지막 요소의 경우 1을 뺀 투플의 길이까지 올라간다. 또한 음의 인덱스를 사용하여 투플의 끝에서 요소에 접근할 수 있다. 인덱스 -1은 마지막 요소, -2부터 두 번째 마지막 요소 등을 나타낸다.

예를 들어 튜플의 첫 번째, 세 번째 및 마지막 요소를 다음과 같이 액세스할 수 있다.

```python
# Create a tuple of names
names = ("Alice", "Bob", "Charlie", "David", "Eve")
print(names)

# Access the first element by index 0
print(names[0])
# Output: Alice

# Access the third element by index 2
print(names[2])
# Output: Charlie

# Access the last element by index -1
print(names[-1])
# Output: Eve
```

또한 start 인덱스와 end 인덱스로 튜플의 슬라이스를 액세스하기 위해 콜론`:`과 두 개의 숫자를 사용할 수 있다. 슬라이스는 start 인덱스의 요소를 포함하지만 end 인덱스의 요소를 제외한다. 튜플의 처음부터 시작하려면 시작 인덱스를 생략할 수 있으며 튜플의 끝에서 종료하려면 종료 인덱스를 생략할 수 있다.

예를 들어 튜플의 처음 세 개, 마지막 두 개, 중간 요소를 다음과 같이 액세스할 수 있다.

```python
# Create a tuple of names
names = ("Alice", "Bob", "Charlie", "David", "Eve")
print(names)

# Access the first three elements by slice 0:3
print(names[0:3])
# Output: ("Alice", "Bob", "Charlie")

# Access the last two elements by slice -2:
print(names[-2:])
# Output: ("David", "Eve")

# Access the middle elements by slice 1:-1
print(names[1:-1])
# Output: ("Bob", "Charlie", "David")
```

튜플의 모든 n번째 마다 요소를 액세스하고 싶다면 콜론 다음의 세 번째 숫자를 사용할 수 있으며, 이를 step이라고 한다. 이 step은 슬라이스의 각 요소 사이에서 몇 개의 요소를 건너뛸지 결정한다. 예를 들어 튜플의 모든 두 번째 요소를 다음과 같이 액세스할 수 있다.

```python
# Create a tuple of names
names = ("Alice", "Bob", "Charlie", "David", "Eve")
print(names)

# Access every second element by slice ::2
print(names[::2])
# Output: ("Alice", "Charlie", "Eve")
```

중첩 튜플에 접근하고자 하는 경우 쉼표로 구분된 여러 인덱스나 슬라이스를 사용할 수 있다. 예를 들어 다음과 같은 중첩 튜플에서 두 번째 튜플의 첫 번째 요소에 접근할 수 있다.

```python
# Create a nested tuple of dates
dates = ((2023, 1, 1), (2023, 2, 14), (2023, 3, 17))
print(dates)

# Access the first element of the second tuple by index 1, 0
print(dates[1][0])
# Output: 2023
```

## <a name="sec_05"></a> 튜플에서 반복 방법
튜플에서 반복하기 위해 루프와 컴프리헨션(comprehension)을 사용할 수 있다. 루프는 조건이 충족될 때까지 코드 블록을 반복하는 문장이다. 컴프리헨션은 함수나 필터를 적용하여 기존 컬렉션에서 새로운 컬렉션을 생성하는 표현이다.

튜플을 반복하는 가장 일반적인 방법 중 하나는 튜플의 각 요소에 대한 코드 블록을 실행하는 `for` 루프를 사용하는 것이다. `in` 키워드를 사용하여 요소가 튜플에 있는지 확인하고 `len()` 함수를 사용하여 튜플의 길이를 얻을 수 있다. 예를 들어, 다음과 같이 튜플의 각 요소를 인쇄할 수 있다.

```python
# Create a tuple of names
names = ("Alice", "Bob", "Charlie", "David", "Eve")
print(names)

# Print each element using a for loop
for name in names:
    print(name)
# Output: 
# Alice
# Bob
# Charlie
# David
# Eve
```

`range()`와 `len()` 함수를 사용하여 튜플을 인덱스로 반복할 수 있다. `range()` 함수는 0에서 튜플의 길이에서 1을 뺀 수열을 반환한다. 이러한 숫자를 인덱스로 사용하여 튜플 요소를 액세스할 수 있다. 예를 들어, 각 요소와 해당 인덱스를 다음과 같이 인쇄할 수 있다.

```python
# Create a tuple of names
names = ("Alice", "Bob", "Charlie", "David", "Eve")
print(names)

# Print each element and its index using a for loop and range()
for i in range(len(names)):
    print(i, names[i])
# Output: 
# 0 Alice
# 1 Bob
# 2 Charlie
# 3 David
# 4 Eve
```

튜플을 반복하는 또 다른 방법은 조건이 참인 상태에서 코드 블록을 실행하는 `while` 루프를 사용하는 것이다. 변수를 사용하여 현재 인덱스를 추적하고 각 반복에서 1씩 증가시킬 수 있다. 인덱스가 튜플의 길이에 도달하면 `break` 문을 사용하여 루프를 종료할 수도 있다. 예를 들어, 다음과 같이 `"David"`를 만날 때까지 각 요소를 인쇄할 수 있다.

```python
# Create a tuple of names
names = ("Alice", "Bob", "Charlie", "David", "Eve")
print(names)

# Print each element until "David" using a while loop
i = 0 # Initialize the index variable
while i < len(names): # Check if the index is within the tuple length
    print(names[i]) # Print the element at the current index
    if names[i] == "David": # Check if the element is "David"
        break # Exit the loop
    i += 1 # Increment the index by 1
# Output: 
# Alice
# Bob
# Charlie
# David
```

튜플을 반복하는 보다 간결한 방법은 컴프리헨션를 사용하는 것인데, 이는 기존의 튜플에서 새로운 컬렉션을 만드는 한 줄 표현식이다. 튜플 컴프리헨션를 사용하여 함수나 필터를 적용하여 기존의 튜플에서 새로운 튜플을 만들 수 있다. 튜플 컴프리헨션는 다음과 같은 구문을 가진다.

```python
# Tuple comprehension syntax
    new_tuple = (expression for element in old_tuple if condition)
```

`expression`은 이전 튜플의 각 요소에 적용하려는 함수나 연산이다. `element`는 튜플 `old_tuple`의 각 요소를 나타내는 변수이다. `old_tuple`은 반복하려는 기존 튜플이다. `condition`은 이전 튜플에서 특정 요소만 선택할 수 있는 선택적 필터이다.

예를 들어 튜플 커프리헨션 사용하여 다음과 같은 기존 이름 튜플에서 대문자 이름의 새로운 튜플을 만들 수 있다.

```python
# Create a tuple of names
names = ("Alice", "Bob", "Charlie", "David", "Eve")
print(names)

# Create a new tuple of uppercase names using a tuple comprehension
upper_names = (name.upper() for name in names)
print(upper_names)
# Output: ("ALICE", "BOB", "CHARLIE", "DAVID", "EVE")
```

또한 튜플 컴프리헨션을 사용하여 다음과 같은 기존 튜플에서 "C"로 시작하는 새로운 튜플을 만들 수 있다.

```python
# Create a tuple of names
names = ("Alice", "Bob", "Charlie", "David", "Eve")
print(names)

# Create a new tuple of names that start with "C" using a tuple comprehension
c_names = (name for name in names if name.startswith("C"))
print(c_names)
# Output: ("Charlie",)
```

이제 루프와 컴프리헨션을 사용하여 튜플을 반복하는 방법을 알게 되었다. 다음에는 튜플을 조작하고 비교하기 위해 튜플 메소드과 연산을 사용하는 방법을 알아보자.

## <a name="sec_06"></a>튜플 메소드와 연산
튜플에는 `count()`와 `index()`의 두 가지 메소드가 내장되어 있으며, 이를 조작하는 데 사용할 수 있다. `count()` 메소드는 튜플에서 지정된 값이 발생한 횟수를 반환한다. `index()` 메소드는 튜플에서 지정된 값이 처음 발생한 위치를 반환한다. 두 메소드 모두 값을 인수로 받아 정수를 반환한다.

예를 들어 `count()` 메서드를 사용하여 튜플에 `"Alice"`가 몇 번 나타나는지 다음과 같이 셀 수 있다.

```python
# Create a tuple of names
names = ("Alice", "Bob", "Alice", "Charlie", "David", "Eve", "Alice")
print(names)

# Count how many times "Alice" appears in the tuple
print(names.count("Alice"))
# Output: 3
```

`index()` 메서드를 사용하여 튜플에서 `"Alice"`가 처음 발생한 위치를 다음과 같이 찾을 수도 있다.

```python
# Create a tuple of names
names = ("Alice", "Bob", "Alice", "Charlie", "David", "Eve", "Alice")
print(names)

# Find the position of the first occurrence of "Alice" in the tuple
print(names.index("Alice"))
# Output: 0
```

찾는 값이 튜플에 없으면 `count()` 메소드는 `0`을 반환하고 `index()` 메소드는 `ValueError` 예외를 발생시킨다. 예를 들어 다음과 같은 오류가 발생한다.

```python
# Create a tuple of names
names = ("Alice", "Bob", "Alice", "Charlie", "David", "Eve", "Alice")
print(names)

# Try to find the position of "Zoe" in the tuple
print(names.index("Zoe"))
# Output: ValueError: tuple.index(x): x not in tuple
```

튜플은 내장된 메소드 외에도 조작하고 비교하는 데 사용할 수 있는 몇 가지 일반적인 연산을 지원한다. 이러한 연산은 다음과 같다.

- **연결(Concatenation)**: `+` 연산자를 사용하여 두 개 이상의 튜플을 새로운 튜플로 연결할 수 있다. 예를 들어, 다음과 같이 두 개의 튜플을 연결하여 새로운 이름의 튜플을 만들 수 있다.

```python
# Create two tuples of names
names1 = ("Alice", "Bob", "Charlie")
names2 = ("David", "Eve", "Frank")
print(names1)
print(names2)

# Concatenate the two tuples into a new tuple
names3 = names1 + names2
print(names3)
# Output: ("Alice", "Bob", "Charlie", "David", "Eve", "Frank")
```

- **반복(Repetition)**: `*` 연산자를 사용하여 지정된 횟수만큼 튜플을 반복하여 새로운 튜플을 만들 수 있다. 예를 들어, 다음과 같이 튜플을 세 번 반복하여 새로운 수의 튜플을 만들 수 있다.

```python
# Create a tuple of numbers
numbers = (1, 2, 3)
print(numbers)

# Repeat the tuple three times and create a new tuple
numbers2 = numbers * 3
print(numbers2)
# Output: (1, 2, 3, 1, 2, 3, 1, 2, 3)
```

- **Membership**: `in`과 `not` 연산자를 사용하여 값이 튜플에 있는지 여부를 확인할 수 있다. 이러한 연산자는 `True` 또는 `False`의 부울 값을 반환한다. 예를 들어, `"Alice"`가 튜플에 있는지 여부를 다음과 같이 확인할 수 있다.

```python
# Create a tuple of names
names = ("Alice", "Bob", "Charlie", "David", "Eve")
print(names)

# Check if "Alice" is in the tuple
print("Alice" in names)
# Output: True

# Check if "Alice" is not in the tuple
print("Alice" not in names)
# Output: False
```

- **비교(Comparison)**: 비교 연산자 `==`, `!=`, `<`, `<=`, `>`, `>=`를 사용하여 값을 기준으로 두 튜플을 비교할 수 있다. 이러한 연산자는 `True` 또는 `False`의 부울 값을 반환한다. 예를 들어, 다음과 같은 두 튜플의 수를 비교할 수 있다.

```python
# Create two tuples of numbers
numbers1 = (1, 2, 3)
numbers2 = (4, 5, 6)
print(numbers1)
print(numbers2)

# Compare the two tuples using comparison operators
print(numbers1 == numbers2) # Check if the tuples are equal
# Output: False
print(numbers1 != numbers2) # Check if the tuples are not equal
# Output: True
print(numbers1 < numbers2) # Check if the first tuple is less than the second tuple
# Output: True
print(numbers1 <= numbers2) # Check if the first tuple is less than or equal to the second tuple
# Output: True
print(numbers1 > numbers2) # Check if the first tuple is greater than the second tuple
# Output: False
print(numbers1 >= numbers2) # Check if the first tuple is greater than or equal to the second tuple
# Output: False
```

이제 튜플을 조작하고 비교하기 위해 튜플 메소드과 연산을 사용하는 방법을 알게 되었다. 

## <a name="conclusion"></a>맺는말
이 자습서에서는 Python으로 튜플을 만들고 접근하는 방법을 배웠다. 튜플은 순서화되고 해시 가능한 값의 불변인 집합이다. 괄호(`()`), 쉼표(`,`), `tuple()` 함수를 사용하여 다양한 타입의 데이터로 튜플을 만드는 방법을 배웠다. 인덱싱, 슬라이싱, 중첩 튜플을 사용하여 튜플 요소에 접근하는 방법도 배웠다.

루프와 커프리헨션을 사용하여 튜플을 반복하는 방법도 배웠다. `for`와 `while` 루프를 사용하여 튜플의 각 요소에 대한 코드 블록을 실행하는 방법도 배웠다. 함수나 필터를 적용하여 기존 튜플에서 새로운 튜플을 만드는 튜플 컴프리헨션 구문 사용법도 배웠다.

마지막으로 튜플을 조작하고 비교하기 위해 튜플 메소드와 연산을 사용하는 방법을 배웠다. 튜플에서 특정 값의 갯수와 위치를 얻기 위해 `count()`와 `index()` 메소드를 사용하는 방법을 배웠다. 연결, 반복, 멤버십, 비교 연산자를 사용하여 튜플에서 합치고, 반복, 확인하고 비교하는 방법도 배웠다.

이 가이드를 따르면 Python 프로그래밍에서 튜플을 사용하는 방법을 잘 이해할 수 있다. 이제 튜플을 사용하여 상수, 구성 설정 또는 레코드와 같이 고정되고 일정한 데이터를 저장하고 처리할 수 있다. 튜플을 사용하여 함수에서 여러 값을 반환하거나 사전에서 키로 사용할 수도 있다.
